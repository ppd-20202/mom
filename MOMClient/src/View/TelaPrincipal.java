/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;

import Models.Chat;
import java.awt.event.ActionEvent;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JList;
import javax.jms.*;
import static momclient.MOMClient.socket;

/**
 *
 * @author isaaccavalcante
 */
public class TelaPrincipal extends javax.swing.JFrame implements MessageListener{
    
    private final String userName;
    PrintStream prtStream;
    
    private final ArrayList<Chat> chats = new ArrayList<>();
    Chat chatToReceiveMessage = null;
    
    private ArrayList<String> listOfQueues = new ArrayList<>();
    private ArrayList<String> listOfTopics = new ArrayList<>();
    private ArrayList<String> listOfSubscriptedTopics = new ArrayList<>();
    
    int index = 0;
    boolean queueSelected = false;
    boolean topicSelected = false;
    boolean topicNotSubscribedSelected = false;
    boolean chatFound = false; 
    boolean queueAlreadyExists = false;

    /**
     * Creates new form TelaPrincipal
     * @param userName
     */
    public TelaPrincipal(String userName) {
        try {
            this.prtStream = new PrintStream(momclient.MOMClient.socket.getOutputStream());
        } catch (IOException ex) {
            Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        initComponents();
        
        //Ação de disparo do envento. Chama método sendMessage(). Se tivesse criado o evento pelos eventos do Netbeans seria sempre enviado um /n que pularia a linha.
        Action enter = new AbstractAction()
        {
            @Override
            public void actionPerformed(ActionEvent ae) {
                sendMessage();
            }
        };
        
        //relaciono o evento criado anteriormento a tecla "Enter"
        messageToSend.getActionMap().put("insert-break", enter);
        
        this.userName = userName;
        
        socketThread();
        sendComand("userName/"+userName);
        
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent winEvt) {
                sendComand("exit/" + userName);
                System.out.println("Saiu");
            }
        });
    }
    
    private void socketThread() {
        Thread t = new Thread(new Runnable() {

            String message = "";

            @Override
            public void run() {

                try {
                    InputStreamReader inputSR = new InputStreamReader(socket.getInputStream());
                    BufferedReader bufReader = new BufferedReader(inputSR);

                    while ((message = bufReader.readLine()) != null) {
                        
                        String[] comands = message.split("/");
                        
                        if("newQueue".equals(comands[0])){
                            listOfQueues.forEach(queue ->{
                               if(queue == null ? comands[1] == null : queue.equals(comands[1])){
                                   queueAlreadyExists = true;
                               } 
                            });
                            if(!queueAlreadyExists){
                                listOfQueues.add(comands[1]);
                                chats.add(new Chat(comands[1], ""));
                                Object[] objArr = listOfQueues.toArray(); 
                                String[] queues = Arrays.copyOf(objArr, objArr.length, String[].class);
                                queueList.setListData(queues);
                            }
                            queueAlreadyExists = false;
                        }
                        if("removeQueue".equals(comands[0])){
                            listOfQueues.remove(comands[1]);
                            Object[] objArr = listOfQueues.toArray(); 
                            String[] queues = Arrays.copyOf(objArr, objArr.length, String[].class);
                            queueList.setListData(queues);
                        }
                        if("newTopic".equals(comands[0])){
                            listOfTopics.add(comands[1]);
                            chats.add(new Chat(comands[1], ""));
                            Object[] objArr = listOfTopics.toArray(); 
                            String[] topics = Arrays.copyOf(objArr, objArr.length, String[].class);
                            listOfNotSubscribedTopics.setListData(topics);
                        }
                        if("removeTopic".equals(comands[0])){
                            listOfTopics.remove(comands[1]);
                            Object[] objArr = listOfTopics.toArray(); 
                            String[] topics = Arrays.copyOf(objArr, objArr.length, String[].class);
                            listOfNotSubscribedTopics.setListData(topics);
                            
                            listOfSubscriptedTopics.remove(comands[1]);
                            objArr = listOfSubscriptedTopics.toArray(); 
                            topics = Arrays.copyOf(objArr, objArr.length, String[].class);
                            topicsList.setListData(topics);
                        }
                        if("newMessage".equals(comands[0])){
                            showChatFromReceivedQueueMessage(comands[1], comands[2] + "\n");
                        }
                    }
                } catch (IOException e) {
                }

            }
        });

        t.start();
    }
    
    private int indexFromList(java.awt.event.MouseEvent evt){
        JList list = (JList)evt.getSource();
        int indexFound = list.locationToIndex(evt.getPoint());
        return indexFound;
    }
    
    private void subscribeInTopic(String topicName) throws JMSException{
        sendComand("subscribe" + "/" + topicName);
    }
    
    private void showChatFromReceivedQueueMessage(String queue, String message){
        getChatToReceiveMesseges(queue);
        
        chatToReceiveMessage.addMessage(message);
        receivedMessages.setText(chatToReceiveMessage.getMessages());
    }
    
    private void getChatToReceiveMesseges(String identifier){
        chats.forEach(chat->{
            if (identifier == null ? chat.getIdentifier() == null : identifier.equals(chat.getIdentifier())) {
                chatFound = true;
                chatToReceiveMessage = chat;
            }
        });
        if(!chatFound) {
            Chat newChat = new Chat(identifier, "");
            chats.add(newChat);
            chatToReceiveMessage = newChat;
        }
    }
    
    
    private void sendMessage() {
        if(queueSelected || topicSelected){
            String message = "";
            String visualPatternMessage = userName + ": " + messageToSend.getText() + "\n";
            if(queueSelected){
                message += "queue/" + listOfQueues.get(index) + "/";
            }
            if(topicSelected){
                message += "topic/" + listOfSubscriptedTopics.get(index) + "/";
            }
            message += visualPatternMessage;
            
            sendComand(message);
            
            showChatFromReceivedQueueMessage(listOfQueues.get(index), visualPatternMessage);

            messageToSend.setText("");
            messageToSend.setCaretPosition(0);
        }
    }
    
    private void sendComand(String comand) {
        prtStream.println(comand);
        prtStream.flush();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        receivedMessages = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        messageToSend = new javax.swing.JTextArea();
        jScrollPane3 = new javax.swing.JScrollPane();
        queueList = new javax.swing.JList<>();
        jScrollPane4 = new javax.swing.JScrollPane();
        topicsList = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        subscribeTopicButton = new javax.swing.JButton();
        sendMessageButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        listOfNotSubscribedTopics = new javax.swing.JList<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        receivedMessages.setEditable(false);
        receivedMessages.setColumns(20);
        receivedMessages.setRows(5);
        receivedMessages.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane1.setViewportView(receivedMessages);

        messageToSend.setColumns(20);
        messageToSend.setRows(5);
        jScrollPane2.setViewportView(messageToSend);

        queueList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        queueList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                queueListMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(queueList);

        topicsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        topicsList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                topicsListMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(topicsList);

        jLabel1.setText("Filas");

        jLabel2.setText("Tópicos");

        subscribeTopicButton.setText("Inscrever-se");
        subscribeTopicButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subscribeTopicButtonActionPerformed(evt);
            }
        });

        sendMessageButton.setText("Enviar");
        sendMessageButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendMessageButtonActionPerformed(evt);
            }
        });

        jLabel3.setText("Tópicos Não Inscritos");

        listOfNotSubscribedTopics.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        listOfNotSubscribedTopics.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                listOfNotSubscribedTopicsMouseClicked(evt);
            }
        });
        jScrollPane5.setViewportView(listOfNotSubscribedTopics);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 390, Short.MAX_VALUE)
                    .addComponent(jScrollPane2)
                    .addComponent(sendMessageButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(subscribeTopicButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 238, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sendMessageButton, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(subscribeTopicButton, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void queueListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_queueListMouseClicked
        index = indexFromList(evt);
        if(index >= 0){
            queueSelected = true;
            topicSelected = false;
            topicNotSubscribedSelected = false;
            String choosedQueueName = listOfQueues.get(index);
            getChatToReceiveMesseges(choosedQueueName);
            receivedMessages.setText(chatToReceiveMessage.getMessages());    
        }
        
    }//GEN-LAST:event_queueListMouseClicked

    private void topicsListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topicsListMouseClicked
        index = indexFromList(evt);
        if(index >= 0){
            queueSelected = false;
            topicSelected = true;
            topicNotSubscribedSelected = false;
            String choosedTopicName = listOfSubscriptedTopics.get(index);
            getChatToReceiveMesseges(choosedTopicName);
            receivedMessages.setText(chatToReceiveMessage.getMessages());
        }
    }//GEN-LAST:event_topicsListMouseClicked

    private void subscribeTopicButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subscribeTopicButtonActionPerformed
        if(topicNotSubscribedSelected){
            try {
                
                subscribeInTopic(listOfTopics.get(index));
                listOfSubscriptedTopics.add(listOfTopics.get(index));
                listOfTopics.remove(index);
                
                Object[] objArr = this.listOfTopics.toArray(); 
                String[] topics = Arrays.copyOf(objArr, objArr.length, String[].class);

                listOfNotSubscribedTopics.setListData(topics);
                
                objArr = this.listOfSubscriptedTopics.toArray(); 
                topics = Arrays.copyOf(objArr, objArr.length, String[].class);
                topicsList.setListData(topics);
            } catch (JMSException ex) {
                Logger.getLogger(TelaPrincipal.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_subscribeTopicButtonActionPerformed

    private void sendMessageButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendMessageButtonActionPerformed
        sendMessage();
    }//GEN-LAST:event_sendMessageButtonActionPerformed

    private void listOfNotSubscribedTopicsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_listOfNotSubscribedTopicsMouseClicked
        queueSelected = false;
        topicSelected = false;
        topicNotSubscribedSelected = true;
        
        index = indexFromList(evt);
    }//GEN-LAST:event_listOfNotSubscribedTopicsMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        
        
    }
    
    @Override
    public void onMessage(Message msg) {
        if(msg instanceof TextMessage){
             try{
                 System.out.println(msg);
                 System.out.println( ((TextMessage)msg).getText());
             }catch(Exception e){
             }
         }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JList<String> listOfNotSubscribedTopics;
    private javax.swing.JTextArea messageToSend;
    private javax.swing.JList<String> queueList;
    private javax.swing.JTextArea receivedMessages;
    private javax.swing.JButton sendMessageButton;
    private javax.swing.JButton subscribeTopicButton;
    private javax.swing.JList<String> topicsList;
    // End of variables declaration//GEN-END:variables

    
}
