/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author isaaccavalcante
 */
public class Chat {
    public String identifier;
    private String messages;
    
    public Chat(String identifier, String messages){
        this.identifier = identifier;
        this.messages = messages;
    }

    public String getMessages() {
        return messages;
    }

    public void addMessage(String message) {
        this.messages += message;
    }
    

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }
}
