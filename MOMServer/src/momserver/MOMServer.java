/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package momserver;

import View.ServerView;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Set;
import javax.jms.JMSException;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.advisory.DestinationSource;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import Models.Queue;
import Models.Topic;
import Models.User;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Destination;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.QueueBrowser;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import org.apache.activemq.command.ActiveMQTextMessage;

/**
 *
 * @author isaaccavalcante
 */
public class MOMServer implements MessageListener{
    
    private final ActiveMQConnectionFactory connectionFactory;
    private final ActiveMQConnection connection;
    private DestinationSource ds;
    
    static Socket socket;
    private ArrayList<Queue> queues = new ArrayList<>();
    private ArrayList<Topic> topics = new ArrayList<>();
    private ArrayList<User> users;
    
    Map<String,ArrayList<User>> topicsWithSubscribers = new HashMap<>();
    
    User userFound;
    Queue queueFound;
    
    Queue queueToDestroy;
    Topic topicToDestroy;
    
    boolean userToDestroyFound = false;
    boolean queueToDestroyFound = false;
    boolean topicToDestroyFound = false;
    
    ServerView view;
    
    boolean queueOrTopicAlreadyRegistered = false;

    /**
     * @param server
     * @throws java.io.IOException
     * @throws javax.jms.JMSException
     */
    public MOMServer(ServerSocket server) throws IOException, JMSException {
        this.users = new ArrayList<>();
        // Create a ConnectionFactory
        connectionFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");

        // Create a Connection
        connection = (ActiveMQConnection) connectionFactory.createConnection();


        connection.start();

        ds = connection.getDestinationSource();
        Set<ActiveMQQueue> queuesToGetName = ds.getQueues();
        Set<ActiveMQTopic> topicsToGetName = ds.getTopics();
 
        ArrayList<String> queuesNames = new ArrayList<>();
        ArrayList<String> topicsNames = new ArrayList<>();
        
        queuesToGetName.forEach((ActiveMQQueue queue) -> {
            try {
                queuesNames.add(queue.getQueueName());
                
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                Destination destination = session.createQueue(queue.getQueueName());
                MessageConsumer consumer = session.createConsumer(destination);
                queues.add(new Queue(queue.getQueueName(), session, destination, consumer, null));
            } catch (JMSException e) {
            }
        });
        topicsToGetName.forEach((ActiveMQTopic topic) -> {
            try {
                topicsNames.add(topic.getTopicName());
                
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                Destination destination = session.createTopic(topic.getTopicName());
                topics.add(new Topic(topic.getTopicName(), session, destination));
            } catch (JMSException e) {
            }
        });
        
        view = new ServerView(queuesNames, topicsNames, this);
        view.setVisible(true);
        view.setResizable(false);
        
        while(true){
            socket = server.accept();
            users.add(new User(socket, this, new PrintStream(socket.getOutputStream())));
        }
    }

    public ArrayList<Queue> getQueues() {
        return queues;
    }

    public ArrayList<Topic> getTopics() {
        return topics;
    }
    
    public int messagesInQueue(String queueName) throws JMSException{
        int messagesCount = 0;
        
        QueueSession queueSession = connection.createQueueSession(true, Session.CLIENT_ACKNOWLEDGE);
        javax.jms.Queue queue = queueSession.createQueue(queueName);
        QueueBrowser browser = queueSession.createBrowser(queue);
        Enumeration<?> messagesInQueue = browser.getEnumeration();

        while (messagesInQueue.hasMoreElements()) {
            Message queueMessage = (Message) messagesInQueue.nextElement();
            messagesCount++;
        }
        return messagesCount;
    }
    
    public void destroyQueue(String queueName) throws JMSException{
        
        queues.forEach((Queue queue) -> {
            if(queue.getName() == null ? queueName == null : queue.getName().equals(queueName)){
                queueToDestroyFound = true;
                queueToDestroy = queue;
            }
        });
        
        Session sessionToDestroy = queueToDestroy.getSession();
        sessionToDestroy.close();
        Destination destination = queueToDestroy.getDestination();
        connection.destroyDestination((ActiveMQDestination) destination);
        
        if(queueToDestroyFound) {
            queues.remove(queueToDestroy);
            queueToDestroyFound = false;
        }
        
        
        users.forEach((User user) -> {
            if(queueName == null ? user.getUserName() != null : !queueName.equals(user.getUserName())){
                user.sendMessage("removeQueue/" + queueName);
            }else{
                userToDestroyFound = true;
                userFound = user;
            }
        });
        
        if(userToDestroyFound) {
            users.remove(userFound);
            userToDestroyFound = false;
        }
        
        view.removeQueue(queueName);
        
    }
    
    public void destroyTopic(String topicName) throws JMSException{
        topics.forEach((Topic topic) -> {
            if(topic.getName() == null ? topicName == null : topic.getName().equals(topicName)){
                topicToDestroyFound = true;
                topicToDestroy = topic;
            }
        });
        Session sessionToDestroy = topicToDestroy.getSession();
        sessionToDestroy.close();
        Destination destination = topicToDestroy.getDestination();
        connection.destroyDestination((ActiveMQDestination) destination);
        
        if(topicToDestroyFound) {
            topics.remove(topicToDestroy);
            topicToDestroyFound = false;
        }
        
        users.forEach((User user) -> {
            user.sendMessage("removeTopic/" + topicName);
        });
        
        view.removeTopic(topicName);
    }
    
    public void addQueue(String queueName) throws JMSException{

        queues.forEach((Queue queue) -> {
            if(queueName.equals(queue.getName())){
                queueFound = queue;
                queueOrTopicAlreadyRegistered = true;
            }
        });
        
        if(!queueOrTopicAlreadyRegistered){
            queueOrTopicAlreadyRegistered = false;
        
            /*
            * Criando Session 
            */		
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            /*
            * Criando Queue
            */
            Destination destination = session.createQueue(queueName);

            /*
            * Criando Consumidor
            */
            MessageConsumer consumer = session.createConsumer(destination);

            view.newQueue(queueName);

            users.forEach((User user) -> {
                
                if(queueName == null ? user.getUserName() != null : !queueName.equals(user.getUserName())){
                    user.sendMessage("newQueue/" + queueName);
                }else{
                    userFound = user;
                }
            });
            
            queues.add(new Queue(queueName, session, destination, consumer, userFound));
        }else{
            users.forEach((User user) -> {
                if(queueFound.getName() == null ? user.getUserName() == null : queueFound.getName().equals(user.getUserName())){
                    queueFound.setUser(user);
                }
            });
        }
    }
    
    public void addTopic(String topicName) throws JMSException{  
        topics.forEach((Topic topic) -> {
            if(topicName.equals(topic.getName())){
                queueOrTopicAlreadyRegistered = true;
            }
        });
        
        if(!queueOrTopicAlreadyRegistered){
            queueOrTopicAlreadyRegistered = false;
            /*
            * Criando Session 
            */		
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

            /*
            * Criando Topic
            */
            Destination destination = session.createTopic(topicName);

            /*
            * Criando Consumidor
            */
            MessageConsumer subscriber = session.createConsumer(destination);

            /*
            * Setando Listener
            */
            subscriber.setMessageListener(this);
        
            topics.add(new Topic(topicName, session, destination));
            view.newTopic(topicName);
            users.forEach((User user) -> {
                if(user.getUserName() == null ? topicName != null : !user.getUserName().equals(topicName)){
                    user.sendMessage("newTopic/" + topicName);
                }
            });
        }
    }
    
    public void userExit(String queueName) throws JMSException{
        queues.forEach((Queue queue) -> {
            if(queueName.equals(queue.getName())){
                queueFound = queue;
            }
        });
        users.forEach((User user) -> {
            if(queueName.equals(user.getUserName())){
                userFound = user;
            }
        });
        
        Session sessionToDestroy = queueFound.getSession();
        sessionToDestroy.close();
        
        queues.remove(queueFound);
        users.remove(userFound);
    }
    
    public void newMessageToQueue(String queueDestination, String message) throws JMSException{
        /*
        * Criando Session 
        */
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        /*
         * Criando Queue
         */
        Destination destination = session.createQueue(queueDestination);

        /*
         * Criando Produtor
         */		
        MessageProducer producer = session.createProducer(destination);
        TextMessage newMessage = session.createTextMessage(message);

        /*
         * Enviando Mensagem
         */
        producer.send(newMessage);

        producer.close();
        session.close();
    }
    
    public void newMessageToTopic(String topicDestination, String message) throws JMSException{
        /*
         * Criando Session 
         */		
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        /*
         * Criando Topic
         */     
        Destination dest = session.createTopic(topicDestination);

        /*
         * Criando Produtor
         */
        MessageProducer publisher = session.createProducer(dest);

        TextMessage newMessage = session.createTextMessage();
        newMessage.setText(message);


        /*
         * Publicando Mensagem
         */
        publisher.send(newMessage);

        publisher.close();
        session.close();
    }
    
    public void newSubscription(String topicName, User user) throws JMSException{
        ArrayList <User> usersSubscribedInTopic = topicsWithSubscribers.get(topicName);
        
        if(usersSubscribedInTopic == null){
            usersSubscribedInTopic = new ArrayList<>();
        }
        
        usersSubscribedInTopic.add(user);
        
        topicsWithSubscribers.put(topicName, usersSubscribedInTopic);
    }
    
    @Override
    public void onMessage(Message msg) {
        
        if(msg instanceof TextMessage){
            try{
                ActiveMQTextMessage message = (ActiveMQTextMessage)msg;
                ActiveMQDestination destination = message.getDestination();
                String topicName = destination.getPhysicalName();
                System.out.println("NOME DO TÓPICO " + topicName);
                ArrayList <User> usersSubscribedInTopic = topicsWithSubscribers.get(topicName);
                String textMessage = ((TextMessage)msg).getText();
                System.out.println("MENSAGEM PARA O TÓPICO " + textMessage);
                usersSubscribedInTopic.forEach(user->{
                    System.out.println(user.getUserName());
                    user.sendMessage("newMessage" + "/" + topicName + "/" + textMessage);                    
                });
            }catch(Exception e){
            }
        }
    }  
}
