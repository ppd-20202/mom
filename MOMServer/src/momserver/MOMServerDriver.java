/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package momserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.Scanner;
import javax.jms.JMSException;
import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;
import static javax.swing.JOptionPane.showInputDialog;
import static javax.swing.JOptionPane.showMessageDialog;

/**
 *
 * @author isaaccavalcante
 */
public class MOMServerDriver {
    public static void main(String[] args) throws JMSException {
        try{
            Scanner input = new Scanner(System.in);
            int port = 0;
            boolean portConfigurationError = false;
            
            do {
                try {
                    port = Integer.parseInt(showInputDialog(null, "Digite a porta de acesso entre 1000 e 60000", "", PLAIN_MESSAGE));
                } catch (NumberFormatException e) {
                  portConfigurationError = true;
                }
                portConfigurationError = port < 1000 || port > 60000;
                if(portConfigurationError){
                    showMessageDialog(null, "Digite um número de porta válido entre 1000 e 60000", "", ERROR_MESSAGE);
                }
            } while (portConfigurationError);
            
            
            ServerSocket server = new ServerSocket(port);
            
            System.out.println("Serviço rodando na porta " + port);
            
            MOMServer momServer = new MOMServer(server);
        }catch (IOException e){
        }
    }
}
