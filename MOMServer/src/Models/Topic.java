/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import javax.jms.Destination;
import javax.jms.Session;

/**
 *
 * @author isaaccavalcante
 */
public class Topic {
    
    public Topic (String name, Session session, Destination destination){
        this.name = name;
        this.session = session;
        this.destination = destination;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
    private String name;
    private Session session;
    private Destination destination;
    
}
