/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;

/**
 *
 * @author isaaccavalcante
 */
public class Queue {

    public Queue (String name, Session session, Destination destination, MessageConsumer consumer, User user){
        this.name = name;
        this.session = session;
        this.destination = destination;
        this.consumer = consumer;
        this.user = user;
        
        queueThread();
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }
    private String name;
    private Session session;
    private Destination destination;
    private final MessageConsumer consumer;
    private User user;

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }
    
    private void queueThread() {
        Thread t;
        t = new Thread(() -> {
            if(session != null){
                try {

                   while (true) {
                       /*
                       * Recebendo Mensagem
                       */
                       Message message = consumer.receive();

                       if (message instanceof TextMessage) {
                           TextMessage textMessage = (TextMessage) message;
                           String text = textMessage.getText();
                           String comands[] = text.split(":");
                           if(user != null){
                               user.sendMessage("newMessage" + "/" + comands[0] + "/" + text);
                           }
                       }
                   }
                } catch (JMSException ex) {
                }
            }
        });

        t.start();
    }
}
