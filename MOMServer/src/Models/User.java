/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;

/**
 *
 * @author isaaccavalcante
 */
public class User {
    private String userName;
    private final User user;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    private Socket socket;
    private final momserver.MOMServer server;

    public Socket getSocket() {
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public PrintStream getPrtStr() {
        return prtStr;
    }

    public void setPrtStr(PrintStream prtStr) {
        this.prtStr = prtStr;
    }
    private PrintStream prtStr;

    public User(Socket socket, momserver.MOMServer server, PrintStream prtStr) {
        this.socket = socket;
        this.server = server;
        this.prtStr = prtStr;
        user = this;

        Thread();
    }

    private void Thread() {
        Thread t = new Thread(new Runnable() {

            String message = "";

            @Override
            public void run() {

                try {
                    InputStreamReader inputSR = new InputStreamReader(socket.getInputStream());
                    BufferedReader bufReader = new BufferedReader(inputSR);

                    while ((message = bufReader.readLine()) != null) {
                        
                        String[] comands = message.split("/");
                        
                        if("userName".equals(comands[0])){
                            userName = comands[1];
                            server.addQueue(userName);
                            ArrayList <Queue> listOfQueues = server.getQueues();
                            ArrayList <Topic> listOfTopics = server.getTopics();
                            
                            listOfQueues.forEach(queue->{
                                sendMessage("newQueue/" + queue.getName());
                            }); 
                            
                            listOfTopics.forEach(topic->{
                                sendMessage("newTopic/" + topic.getName());
                            });     
                        }
                        
                        if("queue".equals(comands[0])){
                            String queueDestination = comands[1];
                            server.newMessageToQueue(queueDestination, comands[2]);
                        }
                        
                        if("topic".equals(comands[0])){
                            String topicDestination = comands[1];
                            server.newMessageToTopic(topicDestination, comands[2]);
                        }
                        
                        if("subscribe".equals(comands[0])){
                            String topicToSubscribe = comands[1];
                            server.newSubscription(topicToSubscribe, user);
                        }
                        
                        if("exit".equals(comands[0])){
                            String queueName = comands[1];
                            server.userExit(queueName);
                        }
                    }
                } catch (IOException e) {
                } catch (JMSException ex) {
                    Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });

        t.start();
    }

    public void sendMessage(String message) {
        if(prtStr != null){
            prtStr.println(message);
            prtStr.flush();
        }
    }
}
